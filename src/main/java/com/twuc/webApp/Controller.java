package com.twuc.webApp;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

/**
 * @author bo.wang
 */
@RestController
@CrossOrigin(value = {"*"})
public class Controller {
    // 跨域问题例子
    @PostMapping("/games")
    public ResponseEntity createNewGame() {
        String[] strings = new String[3];
        int length = strings.length;
        // 生成并存储答案和id
        String answer = "";
        String num = "";
        for (int i = 0; i < 4; i++) {
            while (answer.contains(num)) {
                num = Integer.toString(new Random().nextInt(10));
            }
            answer = answer + num;
        }
        String id = Integer.toString(new Random().nextInt());
        return ResponseEntity
                .status(201)
                .header("Location", "/api/games/" + id)
                .header("Access-Control-Expose-Headers", "Location")
                .contentType(MediaType.TEXT_PLAIN)
                .build();
    }
}
